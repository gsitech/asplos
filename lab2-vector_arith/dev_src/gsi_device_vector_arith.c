/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <stdlib.h>

#include "my_dma.h"
#include "gsi_device_vector_arith.h"

GAL_INCLUDE_INIT_TASK;

/*
 * Welcome to lab 2 of the GSI APU coding session.
 * In this lab, we'll learn how to use the GAL library for I/O operations between the L4/L3 memory to the L2 memory.
 * Then, We'll use GVML functions to move data from L2 to L1 and then to MMB.
 * Once the data is in the MMB, we can utilize the GVML functions and do thousands of parallel operations.
 * Finally, we'll move the outputs the way back from L1 to L4, for the host to access.
 */

GAL_TASK_ENTRY_POINT(vector_arith_task, in, out)
{
	// As in Lab1 - cas the input pointer to common_struct_type. Notice the struct is different in this lab.
	struct common_dev_host *cmn_handle = (struct common_dev_host *)in;
	// Access the common struct buffer and read the input from host.
	gsi_info("Running %s", cmn_handle->buffer);
	// gvml_init_once() must be invoked once in the beggining of a program, for GVML internal initializations.
	gvml_init_once();

	// In order to access pointers we use gal_mem_handle_to_apu_ptr(), same as Lab 1.
	u16 *A_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->in_mem_hndl1);
	u16 *B_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->in_mem_hndl2);

	u16 *X_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->out_mem_hndl1);
	u16 *Y_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->out_mem_hndl2);
	u16 *Z_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->out_mem_hndl3);

	/*
	 * enum gvml_vr16 specifies the different Vector Registers (VRs) in the MMB. Each vector register holds 32K elements of 16 bits.
	 * Storing bigger elements, e.g. 32bit elements, will require multiple VRs.
	 * There are 15 VRs accessible to the user, and 9 VRs are reserved for the GVML internal use.
	 * See resources/libgvml_ref.h for more information.
	 */
	enum gvml_vr16 A_vr, B_vr, X_vr, Y_vr, Z_vr;
	A_vr = GVML_VR16_0;
	// Assign 4 distinct VRs to B, X, Y and Z;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * enum gvml_vm_reg specifies the different Vector Memory Registers (VMRs) in the L1.
	 * Each vector register holds 32K elements of 16 bits.
	 * There are 48 VMRs in total, all of them are accessible to the user.
	 * See resources/libgvml_ref.h for more information.
	 */

	enum gvml_vm_reg vm_reg0, vm_reg1, vm_reg2;
	vm_reg0 = GVML_VM_0;
	// Assign 2 distinct VRs to vm_reg1 and vm_reg2:

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Next, we load the input data pointed to by A_l4 and B_l4 from the L4 to the L2.
	 * Such data transfers are done by the L2DMA. There are 2 such DMAs in every core, one for every APC.
	 * For simplicity, we have written the DMA functions for you in my_gal_dma.h file. You are more than welcome to see the gal function used in it.
	 * Use the my_dma_l4_to_l2_32k() function to send a transfer instruction to the DMA.
	 * Start with the first input, A_l4.
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Now that the data of the first input, A_l4, is in the L2. Move it to the L1 VMR of your choosing using my_dma_l2_to_l1_32k()
	 * Notice this function uses GVML functions.
	 * As a rule, GAL only operates from the L2 up. GVML operates from the L2 down (L2<->L1<->MMB I/O, and MMB operations).
	 * Next, transfer the data from the L1 VMR to the MMB VR "A_vr" using gvml_load_16(), see documentation in resources/gvml_ref.h
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Great, now we have the first input in the MMB. Let's go over the same step and insert the second input to the VR "B" via another VMR
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Use the GVML functions to do some basic arithmetic between the vector elements,
	 * and compute the desired outputs X, Y and Z.
	 * See resources/gvml_ref.h for more information.
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// X = A + B + 5:

	// Y = B - A:

	// Z = A * B:

	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Finally we store the output from the MMB to L1 and than to L4 so the host can access it.
	gvml_store_16(vm_reg2, X_vr);
	my_dma_l1_to_l4(X_l4, 1, vm_reg2);
	gvml_store_16(vm_reg2, Y_vr);
	my_dma_l1_to_l4(Y_l4, 1, vm_reg2);
	gvml_store_16(vm_reg2, Z_vr);
	my_dma_l1_to_l4(Z_l4, 1, vm_reg2);

	/*
	 * End of lab 2. Try compiling your code and running it on the the APU server.
	 * For any questions, please contact the guides.
	 */

	return 0; // success
}
