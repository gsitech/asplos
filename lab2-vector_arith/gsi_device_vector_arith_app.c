/*
 * Copyright (C) 2022, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "gsi_device_vector_arith.h"

GDL_TASK_DECLARE(vector_arith_task);

enum { NUM_CTXS = 1 };
enum { VR_SIZE = 32768 };

/*
 * Welcome to lab 2 of GSI APU coding session.
 * In this lab, we'll learn to utilize the GSI-APU-Library(GAL) to insert and extract data to/from the APU's near memory- the L2.
 * Furthermore, the GSI-Vector-Math-Library (GVML) for data movement from the L2 to the L1 and from there to the MMB.
 * GVML also includes many processing functions for data in the MMB. We'll get to know some of them.
 * The host function below is complete. You are welcome to go through it and see the structure, but do not change the code.
 * The program allocates memory for 2 inputs and 3 outputs. Each of size of a Vector Register (VR) i.e., 32K elements of 16 bits.
 * The inputs are initialized randomly, and the outputs are read after the task is executed.
 * Go to vector_arith-module.c to continue the lab.
 */

int run_vector_arith(gdl_context_handle_t ctx_id, uint16_t *out1, uint16_t *out2, uint16_t *out3, uint16_t *in1, uint16_t *in2)
{
	int ret = 0;
	// allocate common struct and derefence to host pointer
	size_t buf_size = sizeof(struct common_dev_host);
	gdl_mem_handle_t cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct common_dev_host *cmn_handle = gdl_mem_handle_to_host_ptr(cmn_struct_mem_hndl);
	// Allocate data in L4 for all input and outputs
	uint vr_size_in_bytes = VR_SIZE * sizeof(uint16_t);
	cmn_handle->in_mem_hndl1 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	cmn_handle->in_mem_hndl2 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	cmn_handle->out_mem_hndl1 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	cmn_handle->out_mem_hndl2 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	cmn_handle->out_mem_hndl3 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	strcpy(cmn_handle->buffer, "task of vector arith");
	// Store data in L4
	gdl_mem_cpy_to_dev(cmn_handle->in_mem_hndl1, in1, vr_size_in_bytes);
	gdl_mem_cpy_to_dev(cmn_handle->in_mem_hndl2, in2, vr_size_in_bytes);

	// Start task
	const gdl_task_code_offset_t task = GDL_TASK(vector_arith_task);
	ret = gdl_run_task_timeout(ctx_id, task,
							   cmn_struct_mem_hndl, GDL_MEM_HANDLE_NULL,
							   GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
							   GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
							   200, GDL_USER_MAPPING
							   );
	if (ret) {
		printf("Error during task. Error no. %d",ret);
		if (ret == -110) {
			printf(" (time out)");
		}
		putchar('\n');
	}
	gdl_mem_cpy_from_dev(out1, cmn_handle->out_mem_hndl1, vr_size_in_bytes);
	gdl_mem_cpy_from_dev(out2, cmn_handle->out_mem_hndl2, vr_size_in_bytes);
	gdl_mem_cpy_from_dev(out3, cmn_handle->out_mem_hndl3, vr_size_in_bytes);

	// Free memory handles
	gdl_mem_free(cmn_handle->in_mem_hndl1);
	gdl_mem_free(cmn_handle->in_mem_hndl2);
	gdl_mem_free(cmn_handle->out_mem_hndl1);
	gdl_mem_free(cmn_handle->out_mem_hndl2);
	gdl_mem_free(cmn_handle->out_mem_hndl3);
	gdl_mem_free(cmn_struct_mem_hndl);
	return ret;
}

static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};

int main(int GSI_UNUSED(argc), char *argv[])
{
	int ret = 0;
	unsigned int num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];
	gsi_libsys_init(argv[0], true);

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);
	// initilize data randomly
	uint16_t *a, *b;
	a = malloc(VR_SIZE * sizeof(uint16_t));
	b = malloc(VR_SIZE * sizeof(uint16_t));
	for (size_t i = 0; i < VR_SIZE; i++) {
		a[i] = (uint16_t)random();
		b[i] = (uint16_t)random();
	}
	uint16_t *x, *y, *z;
	int x_ok = 0, y_ok = 0, z_ok = 0;
	x = malloc(VR_SIZE * sizeof(uint16_t));
	y = malloc(VR_SIZE * sizeof(uint16_t));
	z = malloc(VR_SIZE * sizeof(uint16_t));
	unsigned ctx_id = 0;
	for (ctx_id = 0; ctx_id < num_ctxs; ctx_id++) {
		ret = gdl_context_alloc(contexts_desc[ctx_id].ctx_id, 0, NULL, NULL);
		if (ret) {
			// printf("Failed to allocate GDL context (err = %d)!!!\n", ret);
			continue;
		}
		printf("Running on card number %u\n", ctx_id);
		ret = run_vector_arith(contexts_desc[ctx_id].ctx_id, x, y, z, a, b);
		gdl_context_free(contexts_desc[ctx_id].ctx_id);
		break;
	}
	if (ctx_id == num_ctxs) {
		printf("All cards are currently in use. Please try again.\n If this error persists contact a guide. Sorry for the inconvience.");
	}
	/* Validate results:
	 * x is expected to be a + b + 5
	 * y is expected to be b - a
	 * z is expected to be a * b
	 */
	for (size_t i = 0; i < VR_SIZE; i++) {
		if (!x_ok && x[i] != (uint16_t)(a[i] + b[i] + 5)) {
			x_ok = 1; printf("Error in X computation in element %ld\n", i);
		}
		if (!y_ok && y[i] != (uint16_t)(b[i] - a[i])) {
			y_ok = 1; printf("Error in Y computation in element %ld\n", i);
		}
		if (!z_ok && z[i] != (uint16_t)(a[i] * b[i])) {
			z_ok = 1; printf("Error in Z computation in element %ld\n", i);
		}
	}

	gdl_exit();
	free(a);
	free(b);
	free(x);
	free(y);
	free(z);
	ret = (x_ok | y_ok | z_ok);
	gsi_sim_destroy_simulator();
	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nVector Arith App Failed\n");
	}
	else {
		printf("\nVector Arith App Passed\n");
	}
	return ret;
}
