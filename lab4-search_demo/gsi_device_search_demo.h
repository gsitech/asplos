/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef EXE_COMMON_H
#define EXE_COMMON_H

#define ADDRESS_SIZE		8

struct entry{
	uint16_t		address[ADDRESS_SIZE];
	uint16_t 		data;
}__attribute__((packed));

struct common_dev_host {
	char 			buffer[64];
	// data for entry load
	uint64_t 		in_address_hndl[ADDRESS_SIZE];
	uint64_t 		in_data_hndl;
	// data for entries search
	uint64_t		in_queries_hndl;
	// common data for load and search
	unsigned int 	num_entries;
}__attribute__((packed));

#endif /* EXE_COMMON_H */
