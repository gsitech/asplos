<div id="top"></div>
 
<!-- ABOUT THE PROJECT -->
## About The Project

This project encapsulates the tutorial for ASPLOS including hands-on program exercises for participants.

<!-- GETTING STARTED -->

### Doing labs on the APU server (ex: fractals-2):
Use forticlient VPN to access the APU server.

## Connect to VPN using FortiClient App:
Download [FortiClient VPN](https://www.fortinet.com/tw/support/product-downloads#vpn).
The credentials are as follows:
<div align="center">
  <a >
    <img src="images/forticlient.png" alt="forticlient_credentials" width="30" height="30">
  </a>
</div>

## Connect to VPN using terminal (Ubuntu):
Download the forticlientsslvpn_linux_4 tar:
```
wget http://cdn.software-mirrors.com/forticlientsslvpn_linux_4.4.2328.tar.gz
```

Uncompress the downloaded file:
```
tar -xzvf forticlientsslvpn_linux_4.4.2328.tar.gz
```

Install ppp (in case you don't have it):
```
sudo apt-get install ppp
```

Go to the installer setup directory:
```
cd ./forticlientsslvpn/64bit/helper
```

Run the setup file:
```
sudo ./setup.linux.sh
```

return to forticlientsslvpn/64bit/:
```
cd ..
```

Finally, you can connect whenever you want using this command:
```
./forticlientsslvpn_cli --server 209.36.1.31:10443 --vpnuser username
```
You will be asked for the password

Once connected to the VPN - log into the APU server from the terminal. Example:
```
ssh username@192.168.99.32
```

**For user names and passwords contact us.**

Once logged into the APU server - activate conda environment:

Display list of environments:
```
conda env list
```

Activate the environment:
```
conda activate build_env_x # Where x = 1,2,3,4 (Find correct x in list)
```

Run the following command:
```
source /home/public/env_set.env
```

Next, GIT clone the asplos directory on the APU server:
```
git clone https://bitbucket.org/gsitech/asplos
```

Set the path to the Synopsys MetaWare compilers. Example:
```
source /efs/data/public/synopsis/ARC-2018.06/env_set.env
```

Or if using the GNU toolchain for ARC, then set the path to the GNU toolchain for ARC compiler from the GSI share. Example:
```
export PATH=/efs/data/public/synopsis/GNU/arc_gnu_2023.03-release_elf32_le_linux_no_sdata/arc-snps-elf/bin:${PATH}
```
Or if instaling the GNU toolchain for ARC on a new Ubuntu machine, then:
```
git clone https://bitbucket.org/gsitech/gnu-toolchain-for-arc.git
cd gnu-toolchain-for-arc
git checkout gnu-toolchain-for-arc_releases_2023.03_baremetal_elf32_no-sdata_prebuild-ubuntu1804-amd64
tar xf arc_gnu_2023.03-release_elf32_le_linux_no_sdata.tar.gz
export PATH=${PWD}/arc_gnu_2023.03-release_elf32_le_linux_no_sdata/arc-snps-elf/bin:PATH=${PATH}
```

## Accessing the labs coding exercises on your computer
* GIT clone the repository to your computer:
```
git clone https://bitbucket.org/gsitech/asplos
```

* Enter the first lab directory
```
cd asplos/lab1-hello_world
```

* Open on your IDE the gsi_device_hello_world_app.c (host side) and dev_src/gsi_device_hello_world.c (device side) files.
* Follow the instruction on the host side file.
* Once done on the host side file, go to the device side file and follow the instructions there.
* When done, test your solution using the instructions in the next section.

* Exercise files by lab number:

  1. Lab 1: gsi_device_hello_world_app.c  and dev_src/gsi_device_hello_world.c
  2. Lab 2: gsi_device_vector_arith_app.c and dev_src/gsi_device_vector_arith.c
  3. Lab 3: gsi_device_find_min_max_app.c and dev_src/gsi_device_find_min_max.c
  4. Lab 4: gsi_device_search_demo_app.c  and dev_src/gsi_device_search_demo.c

## Compiling and running your lab solution:
Once you have finished writing the solution to a specific lab. Do the following steps to test the solution correctness:

* Make sure your current directory is the lab's main directory (e.g., \*/asplos/lab1-hello_world)

* Copy the files you have completed to the APU server using the script *copy2server.sh*. You will be required to enter the user password.
Debian OS:
```
./copy2server.sh [username] [APU-server-IP]
```

Windows/Mac OS:
```
sh copy2server.sh [username] [APU-server-IP]
```

If the script does not work, copy the scp command inside it and run it directly.

* In the terminal connected to the APU server. Verify you are in the correct directory (e.g., ~/asplos/lab1-hello_world), and conda environment is active.

* Compile the code - get usage message:
```
make help
```
* Compile the code - clean previous build:
```
make distclean
```
* Compile the code - using the default Synopsys MetaWare ccac compiler for ARC:
```
make V=1
```
* Compile the code for APU Edge:
```
make V=1 product=edge
```
* Compile the code with the GNU toolchain for ARC (instead of the default Synopsys MetaWare ccac compiler):
```
make V=1 product=gnu-ccac
```

* Once compilation succeeds. The executable will be placed in the build directory 'build/debug' under the name gsi_device_LAB where 'LAB' is lab name.

* Run it with the following command:
```
./build/debug/gsi_device_LAB (e.g., ./build/debug/gsi_device_hello_world)
```

* The program output will say if the lab passed or failed.

## Viewing to the APU's output:

Open another terminal tab or window, connect to the APU server using the same username password.

Enter the ledag-ssh interface, communicating to the APU.
```
ledag-ssh -o localhost
```

There are four APU chips on APU server. When you run a test, the program will print to which card was used.

Select the card using:
```
select slot[card_id] (e.g. select 0)
```

View the APU log using the *flo* command
```
flo
```

<!-- CONTACT -->
## Contact

Gal Shalif - gshalif@gsitechnology.com

Project Link: [https://bitbucket.org/gsitech/asplos/](https://bitbucket.org/gsitech/asplos/)

## Full programming course
* If you are interested in research or commercial use of GSI's APU. you are welcome to contact us at Dan Ilan (dilan@gsitechnology.com) or Gal Shalif (gshalif@gsitechnology.com).
We'll be happy to provide you with the GSI's programming course and development environment.


<p align="right">(<a href="#top">back to top</a>)</p>
