/*
 * Copyright (C) 2022, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "gsi_device_find_min_max.h"

GDL_TASK_DECLARE(find_min_max_task);

enum { NUM_CTXS = 1 };
enum { VR_SIZE = 32768 };

/*
 * Welcome to lab 3 of GSI APU coding session.
 * In this lab, you are asked to help develop a new function "APU_find_min_max" for APU users.
 * We will write the function for unsigned shorts (u16), but this can be easily extended to other types.
 * The function will get a vector "vec" of 32K elements of unsigned shorts, and using the APU,
 * will return the maximum and minimum values of the vector.
 *
 * Start by going through the host function "APU_find_min_max" and fill in the blanks.
 * Use the GDL functions described in resources/libgdl_ref.h in your solution.
 */

int APU_find_min_max(gdl_context_handle_t ctx_id, uint16_t* found_min, uint16_t *found_max, uint16_t vec[VR_SIZE])
{
	int ret = 0;
	// allocate common struct and derefence to host pointer
	size_t buf_size = sizeof(struct common_dev_host);
	gdl_mem_handle_t cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct common_dev_host *cmn_handle = gdl_mem_handle_to_host_ptr(cmn_struct_mem_hndl);
	// Allocate data in L4 for all input and outputs
	uint vr_size_in_bytes = VR_SIZE * sizeof(uint16_t);
	cmn_handle->in_mem_hndl1 = gdl_mem_alloc_nonull(ctx_id, vr_size_in_bytes, GDL_CONST_MAPPED_POOL);
	// Write to the common handle some string
	strcpy(cmn_handle->buffer, "task of find limits (min and max)");

	/*
	 * For fast IO with the L4, it is recommened to use the gdl_mem_cpy_to_dev() function, which utilized DMA capabilities of the host.
	 * See documentation in resources/libgdl_ref.h
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	gdl_mem_cpy_to_dev(cmn_handle->in_mem_hndl1, vec, vr_size_in_bytes);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/* Start the task find_min_max_task */
	gdl_task_code_offset_t task;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	task = GDL_TASK(find_min_max_task);
	ret = gdl_run_task_timeout(ctx_id, task,
							   cmn_struct_mem_hndl, GDL_MEM_HANDLE_NULL,
							   GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
							   GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
							   200, GDL_USER_MAPPING
							   );
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	if (ret) {
		printf("Error during task. Error no. %d",ret);
		if (ret == -110) {
			printf(" (time out)");
		}
		putchar('\n');
	}
	/* Store found min and max value in desired places */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	*found_max = cmn_handle->max_found;
	*found_min = cmn_handle->min_found;
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Free memory handles
	gdl_mem_free(cmn_handle->in_mem_hndl1);
	gdl_mem_free(cmn_struct_mem_hndl);
	return ret;

	/*
	 * Great, now that we are all set up on the host side, let's go to the APU side and write the task.
	 * Go to find_min_max-module.c to write the APU task.
	 */
}

static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};

int main(int GSI_UNUSED(argc), char *argv[])
{
	int ret = 0;
	unsigned int num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];
	gsi_libsys_init(argv[0], true);

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);
	// Initiate data randomly
	uint16_t *a = malloc(VR_SIZE * sizeof(uint16_t));
	uint16_t max_val = 0;
	uint16_t min_val = USHRT_MAX;
	uint16_t found_min, found_max;
	for (size_t i = 0; i < VR_SIZE; i++) {
		a[i] = (uint16_t)random() % ((uint16_t)random()+1);
		max_val = MAX(max_val, a[i]);
		min_val = MIN(min_val, a[i]);
	}
	unsigned ctx_id = 0;
	for (ctx_id = 0; ctx_id < num_ctxs; ctx_id++) { // Search for free APU Card
		ret = gdl_context_alloc(contexts_desc[ctx_id].ctx_id, 0, NULL, NULL);
		if (ret) { // not found - search for next
			// printf("Failed to allocate GDL context (err = %d)!!!\n", ret);
			continue;
		}
		printf("Running on card number %u\n", ctx_id);
		// apu card found and allocated. run task and check outputs
		ret = APU_find_min_max(contexts_desc[ctx_id].ctx_id, &found_min, &found_max, a);
		/* Validate results: max_val == found_max ? */
		if (max_val != found_max) {
			ret |= 1;
			printf("Max search failed: Found value = %d, True max = %d\n", found_max, max_val);
		}
		if (min_val != found_min) {
			ret |= 1;
			printf("Min search failed: Found value = %d, True max = %d\n", found_min, min_val);
		}
		// free and break
		gdl_context_free(contexts_desc[ctx_id].ctx_id);
		break;
	}
	if (ctx_id == num_ctxs) {
		printf("All cards are currently in use. Please try again.\n If this error persists contact a guide. Sorry for the inconvience.");
	}
	free(a);
	gdl_exit();

	gsi_sim_destroy_simulator();
	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nFind Max App Failed\n");
	}
	else {
		printf("\nFind Max App Passed\n");
	}
	return ret;
}
