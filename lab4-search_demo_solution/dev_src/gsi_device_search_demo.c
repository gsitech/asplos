/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <gsi/libgvml_get_marked_data.h>
#include <gsi/libgvml_debug.h>
#include <stdlib.h>

#include "my_dma.h"
#include "gsi_device_search_demo.h"

GAL_INCLUDE_INIT_TASK;

/*
 * Welcome to lab 4 of GSI APU coding session.
 * In this lab, we'll create a fully operational search engine.
 * Each search entry consists of 128 bits of "address"(e.g., ipv6 address), and 16 bits of data (e.g., corresponding server number ).
 * For the purpose of the exercise, we consider the 128-bit address as eight elements of 16 unsigned shorts.
 * Complete 2 tasks in this lab:
 * 	a. The first task receives the data base and loads the data to the MMB.
 * 	b. The second task input is a batch of queries, the APU searches the database for the queries and returns the correct data if found.
 *
 * Throughout this lab, refer to resources/libgvml_ref.h for required functions documentation.
 * If you have finished the host side function run_load_entries(), go on to load_entries_task.
 */

GAL_TASK_ENTRY_POINT(load_entries_task, in, out)
{
	// cast the input pointer to common_struct_type. Notice the struct is different in this lab.
	struct common_dev_host *cmn_handle = (struct common_dev_host *)in;
	// Access the common struct buffer and read the input from host.
	gsi_info("Running %s", cmn_handle->buffer);
	// gvml_init_once() must be invoked once in the beggining of a program, for GVML internal initializations.
	gvml_init_once();

	// In order to access pointers we use gal_mem_handle_to_apu_ptr().
	u16 *address_l4[8];
	for (int i = 0; i < ADDRESS_SIZE; i++) {
		address_l4[i] = gal_mem_handle_to_apu_ptr(cmn_handle->in_address_hndl[i]);
	}

	/* convert the memory handle of the data to a pointer to data_l4 */
	u16 *data_l4;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	data_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->in_data_hndl);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Load the addresses and data to the MMB.
	 * Addresses are stored in VRs 0-7. Data is stored in VR 8
	 */
	enum gvml_vr16 address_vr[8] = {
		GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3,
		GVML_VR16_4, GVML_VR16_5, GVML_VR16_6, GVML_VR16_7
	};
	enum gvml_vr16 data_vr = GVML_VR16_8;
	unsigned int num_entries = cmn_handle->num_entries;
	for (int i = 0; i < ADDRESS_SIZE; i++) {	// Load addresses to MMB
		my_dma_l4_to_l2(address_l4[i], num_entries);
		my_dma_l2_to_l1_32k(GVML_VM_0);
		gvml_load_16(address_vr[i], GVML_VM_0);
	}

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Load data to MMB
	my_dma_l4_to_l2(data_l4, num_entries);
	my_dma_l2_to_l1_32k(GVML_VM_0);
	gvml_load_16(data_vr, GVML_VM_0);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * It is possible not all 32K columns store valid data. e.g., if num_entries < 32K.
	 * To avoid false matches in such cases, we set a marker of all valid entries.
	 * Each column has a unique index stored in the GVML_VR16_IDX Vector Register.
	 * We stored our data in the first num_entries columns.
	 * Use gvml_le_imm_u16 to set the marker valid_mrk only in columns with valid data.
	 */
	enum gvml_mrks_n_flgs valid_mrk = GVML_MRK0;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	gvml_le_imm_u16(valid_mrk, GVML_VR16_IDX, cmn_handle->num_entries);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	return 0; // success
	/*
	 * Great, now we have a storing function and task for loading entries to the APU.
	 * Next, we write the search host function and task.
	 * Go to run_search_queries() function in search_demo.c for further instructions.
	 */
}



/*
 * Complete the search_queries_task after writing the host function run_search_queries.
 */
GAL_TASK_ENTRY_POINT(search_queries_task, in, out)
{
	// As in Lab1 - cas the input pointer to common_struct_type. Notice the struct is different in this lab.
	struct common_dev_host *cmn_handle = (struct common_dev_host *)in;
	// Access the common struct buffer and read the input from host.
	gsi_info("Running %s", cmn_handle->buffer);

	// In order to access pointers we use gal_mem_handle_to_apu_ptr(), same as Lab 1.
	struct entry *queries_l4;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	queries_l4 = gal_mem_handle_to_apu_ptr(cmn_handle->in_queries_hndl);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	enum gvml_vr16 address_vr[8] = {
		GVML_VR16_0, GVML_VR16_1, GVML_VR16_2, GVML_VR16_3,
		GVML_VR16_4, GVML_VR16_5, GVML_VR16_6, GVML_VR16_7
	};
	enum gvml_vr16 data_vr = GVML_VR16_8;

	/*
	 * We'll use 3 markers:
	 * a. valid_mrk - The valid marker wich we set in the load task.
	 * b. match_mrk - A marker that indicates the column is a match/miss
	 * c. tmp_mrk 	 - Temporary marker.
	 */
	enum gvml_mrks_n_flgs valid_mrk = GVML_MRK0;
	enum gvml_mrks_n_flgs match_mrk = GVML_MRK1;
	enum gvml_mrks_n_flgs tmp_mrk = GVML_MRK2;

	/*
	 * Loop over all queries, for every query search whether it's address is stored in the MMB.
	 * If it is, store the data of the relevant column in the query's data member.
	 * Use the following GVML functions:
	 * 1. gvml_cpy_m()
	 * 2. gvml_eq_imm_16()
	 * 3. gvml_and_m()
	 * 4. gvml_get_marked_data()
	 */
	for (size_t q = 0; q < cmn_handle->num_entries; q++) {
		u16 found_data = 0;

		// ******** WRITE YOUR CODE UNDER THIS LINE ************************
		gvml_cpy_m(match_mrk, valid_mrk);
		for (size_t i = 0; i < ADDRESS_SIZE; i++) {
			gvml_eq_imm_16(tmp_mrk, address_vr[i], queries_l4[q].address[i]);
			gvml_and_m(match_mrk, match_mrk, tmp_mrk);
		}
		int num_found = gvml_get_marked_data(&found_data, data_vr, 1, match_mrk, 1);
		queries_l4[q].data = found_data;
		if (num_found == 0) { // no matches found
			queries_l4[q].data = -1;
		}
		// ******** WRITE YOUR CODE ABOVE THIS LINE ************************
	}

	return 0; // success
	/*
	 * Perfect, go ahead and compile your code and tests it on the APU server.
	 */
}
