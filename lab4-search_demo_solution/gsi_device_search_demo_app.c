/*
 * Copyright (C) 2022, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "gsi_device_search_demo.h"

GDL_TASK_DECLARE(load_entries_task);
GDL_TASK_DECLARE(search_queries_task);

enum { NUM_CTXS = 1 };
enum { VR_SIZE = 32768 };

/*
 * Welcome to lab 4 of GSI APU coding session.
 * In this lab we'll create a fully operational search engine.
 * Each search entry consists 128 bits of "address"(e.g., ipv6 address), and 16 bits of data (e.g., corresponding server number ).
 * For the purpose of the exercise, we consider the 128 bit address as eight elements of 16 unsigned shorts.
 * In this lab You will build two tasks.
 * a. The first task recieves the data base and loads the data to the MMB.
 * b. The second task get a batch of queries, Search the data base and returns the correct data if found.
 */

/*
 * Load functions:
 * Complete the host code for loading entries to the APU.
 */
int run_load_entries(gdl_context_handle_t ctx_id, struct entry *entries, unsigned int num_entries)
{
	int ret = 0;
	// allocate common struct and derefence to host pointer
	size_t buf_size = sizeof(struct common_dev_host);
	gdl_mem_handle_t cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct common_dev_host *cmn_handle = gdl_mem_handle_to_host_ptr(cmn_struct_mem_hndl);
	strcpy(cmn_handle->buffer, "task of load entries");

	// Allocate data in L4 for all input and outputs
	uint address_size_in_bytes = num_entries * sizeof(uint16_t);

	// For efficient Loading to the MMB, we allocate a separate memory handle for every element of the 8 address elements.
	uint16_t *dev_address[8];
	for (size_t i = 0; i < ADDRESS_SIZE; i++) {
		// Allocate space for the element i of all entries.
		cmn_handle->in_address_hndl[i] = gdl_mem_alloc_nonull(ctx_id, address_size_in_bytes, GDL_CONST_MAPPED_POOL);
		// Convert element i memory handle to pointer
		dev_address[i] = gdl_mem_handle_to_host_ptr(cmn_handle->in_address_hndl[i]);
	}

	/* Allocate memory for the data and store the memory handle in cmn_handle->in_data_hndl, than convert the memory handle to a pointer */
	uint16_t *dev_data;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	cmn_handle->in_data_hndl = gdl_mem_alloc_nonull(ctx_id, address_size_in_bytes, GDL_CONST_MAPPED_POOL);
	dev_data = gdl_mem_handle_to_host_ptr(cmn_handle->in_data_hndl);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Load addresses and data to L4 entries
	for (size_t entry_idx = 0; entry_idx < num_entries; entry_idx++) {
		for (size_t j = 0; j < ADDRESS_SIZE; j++) {
			dev_address[j][entry_idx] = entries[entry_idx].address[j];
		}
		dev_data[entry_idx] = entries[entry_idx].data;
	}
	cmn_handle->num_entries = num_entries;
	// Start "load_entries_task" task
	const gdl_task_code_offset_t task = GDL_TASK(load_entries_task);

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	ret = gdl_run_task_timeout(ctx_id, task,
							   cmn_struct_mem_hndl, GDL_MEM_HANDLE_NULL,
							   GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
							   GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
							   200, GDL_USER_MAPPING
							   );
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	if (ret) {
		printf("Error during task. Error no. %d", ret);
		if (ret == -110) {
			printf(" (time out)");
		}
		putchar('\n');
	}

	// Free memory handles
	for (size_t i = 0; i < ADDRESS_SIZE; i++) {
		gdl_mem_free(cmn_handle->in_address_hndl[i]);
	}
	gdl_mem_free(cmn_handle->in_data_hndl);

	return ret;

	/* Now that we have the host side ready, Move on to the APU side to code the storing task */
}

/*
 * Search function:
 * Complete the code for initiating a search of queries in the APU.
 */
int run_search_queries(gdl_context_handle_t ctx_id, struct entry *queries_sys, unsigned num_queries)
{
	int ret = 0;
	// allocate common struct and derefence to host pointer
	size_t buf_size = sizeof(struct common_dev_host);
	gdl_mem_handle_t cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);
	struct common_dev_host *cmn_handle = gdl_mem_handle_to_host_ptr(cmn_struct_mem_hndl);
	strcpy(cmn_handle->buffer, "task of Search queries");

	// Allocate data in L4 for all input and outputs
	uint querry_size_in_bytes = num_queries * sizeof(struct entry);
	cmn_handle->in_queries_hndl = gdl_mem_alloc_aligned(ctx_id, querry_size_in_bytes, GDL_CONST_MAPPED_POOL, GDL_ALIGN_256);

	/* Copy the queries to L4 using gdl_mem_cpy_to_dev(). If the server has a DMA, it will be utilized for the data transfer. */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	gdl_mem_cpy_to_dev(cmn_handle->in_queries_hndl, queries_sys, querry_size_in_bytes);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Set the number of queries to search
	cmn_handle->num_entries = num_queries;

	// Start task
	const gdl_task_code_offset_t task = GDL_TASK(search_queries_task);
	ret = gdl_run_task_timeout(ctx_id, task,
							   cmn_struct_mem_hndl, GDL_MEM_HANDLE_NULL,
							   GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
							   GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
							   200, GDL_USER_MAPPING
							   );
	if (ret) {
		printf("Error during task. Error no. %d", ret);
		if (ret == -110) {
			printf(" (time out)");
		}
		putchar('\n');
	}
	/*
	 * Convert cmn_handle->in_queries_hndl memory handle to a (struct entry*) pointer.
	 * Store the results data in the data part of the system's queries.
	 */
	struct entry *queries_l4;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	queries_l4 = gdl_mem_handle_to_host_ptr(cmn_handle->in_queries_hndl);
	for (size_t i = 0; i < num_queries; i++) {
		queries_sys[i].data = queries_l4[i].data;
	}
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Free memory handles
	gdl_mem_free(cmn_handle->in_queries_hndl);

	return ret;
	/*
	 * Good job. Nearly done.
	 * Continue to search_queries_task in search_demo-module.c to complete the search task.
	 */
}

static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};
void randomly_init_entries(struct entry *host_entries, unsigned int num_entries)
{
	for (unsigned i = 0; i < num_entries; ++i) {
		// printf("entry %u:\n", i);
		for (unsigned j = 0; j < ADDRESS_SIZE; ++j) {
			host_entries[i].address[j] = (uint16_t)random();
			// printf("%x ", host_entries[i].address[j]);
		}
		host_entries[i].data = i;
		// printf(".\tdata = %u\n", host_entries[i].data);
	}
}

void randomly_init_queries(bool* is_from_entry, struct entry *host_queries, struct entry *host_entries, unsigned num_queries)
{
	int random_threshold = RAND_MAX / 2;
	for (unsigned i = 0; i < num_queries; i++) {
		if (random() < random_threshold) { // assign randomly
			is_from_entry[i] = false;
			for (int j = 0; j < ADDRESS_SIZE; j++) {
				host_queries[i].address[j] = (uint16_t)random();
			}
		}
		else {	// assign the host query
			is_from_entry[i] = true;
			for (int j = 0; j < ADDRESS_SIZE; j++) {
				host_queries[i].address[j] = host_entries[i].address[j];
			}
		}
		host_queries[i].data = -1;
	}
}

int main(int GSI_UNUSED(argc), char *argv[])
{
	int ret = 0;
	unsigned int num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];
	gsi_libsys_init(argv[0], true);

	gsi_sim_create_simulator(NUM_CTXS, g_ctxs);

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);
	unsigned int num_entries = 32760;
	unsigned int num_queries = 500;
	num_queries = MIN(num_queries, num_entries);
	unsigned ctx_id = 0;
	for (ctx_id = 0; ctx_id < num_ctxs; ctx_id++) {
		ret = gdl_context_alloc(contexts_desc[ctx_id].ctx_id, 0, NULL, NULL);
		if (ret) {
			// printf("Failed to allocate GDL context (err = %d)!!!\n", ret);
			continue;
		}
		printf("Running on card number %u\n", ctx_id);
		struct entry *host_entries = malloc(sizeof(struct entry) * num_entries);
		randomly_init_entries(host_entries, num_entries);
		ret = run_load_entries(contexts_desc[ctx_id].ctx_id, host_entries, num_entries);

		struct entry *host_queries = malloc(sizeof(struct entry) * num_entries);
		bool *is_from_entry = malloc(num_queries * sizeof(bool));
		randomly_init_queries(is_from_entry, host_queries, host_entries, num_queries);
		uint16_t false_return_value = -1;
		ret = run_search_queries(contexts_desc[ctx_id].ctx_id, host_queries, num_queries);
		for (unsigned q = 0; q < num_queries; q++) {
			if (is_from_entry[q] && (host_queries[q].data != host_entries[q].data)) {
				printf("Failed to find query #%u (Found %d)\n", q, host_queries[q].data);
				ret = 1;
			}
			else if (!is_from_entry[q] && (host_queries[q].data != false_return_value)) {
				printf("Unexpectedly found query #%u (Found %d)\n", q, host_queries[q].data);
				ret = 1;
			}
		}

		free(host_entries);
		free(host_queries);
		gdl_context_free(contexts_desc[ctx_id].ctx_id);
		break;
	}
	if (ctx_id == num_ctxs) {
		printf("All cards are currently in use. Please try again.\nIf this error persists contact a guide. Sorry for the inconvience.\n");
	}

	gdl_exit();

	gsi_sim_destroy_simulator();
	gsi_libsys_exit();
	if (ret != 0) {
		printf("\nSearch Demo App Failed\n");
	}
	else {
		printf("\nSearch Demo App Passed\n");
	}
	return ret;
}
