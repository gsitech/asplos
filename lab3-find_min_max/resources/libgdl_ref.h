/*
 *
 * The GSI Device Library (GDL) API provides a host interface to enable the building 
 * and running user tasks on the APU board. This API allows communication 
 * between the host (the server where the APU is installed) and the device (the APU board). 
 * 
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 * 
 * 
 */

#ifndef GSI_LIBGDL_H
#define GSI_LIBGDL_H

typedef uintptr_t gdl_context_handle_t;
typedef uintptr_t gdl_mem_handle_t;

typedef enum {
	GDL_USER_MAPPING,
}gdl_mapping_type;

struct gdl_context_desc {
	char parent_device_name[GDL_MAX_DEVICE_NAME_LENGTH];
	gdl_context_handle_t ctx_id;
	unsigned int num_apucs;
	unsigned int num_apus;
	gdl_context_status_t status;
	unsigned long long  mem_size;
	unsigned int hw_general_info;				/* HW General Info Bits - 
													Bit[3:0] Freq:						(2:200 Mhz/3:300 Mhz /4:400 Mhz)
													Bit[5] Indirect Read: 				(0:Disabled/1:Enabled)
													Bit[6] Indirect Write:				(0:Disabled/1:Enabled)
												*/
};


/*
 * gdl_mem_cpy_to_dev - copy from the host to the devices L4 memory.
 *
 * Input:
 *   @dst - memory handle to the devices memory where the content is to be copied
 * 	 @src - source address to copy from
 *	 @size - number of bytes to copy
 * 
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 * 
 * Comments:
 *  The functions uses the hosts DMA in order to copy the data, in case there is no host DMA
 *  or for some reason using the DMA is not possible , normal memcpy is used to copy.
 *  For optimized performance the addresses should be aligned to GDL_MEM_CPY_OPTIMIZED_ALIGNMENT 
 * 
 */
int gdl_mem_cpy_to_dev(gdl_mem_handle_t dst, const void *src, unsigned long long size);

/*
 * gdl_mem_alloc_nonull - allocate a number of bytes requested in the given memory pool of the specified context.
 *
 * Input:
 *   @ctx_handler - the requested hw context to allocate from.
 *   @size - number of bytes to allocate in the memory region . ( size is aligned up to a multiply of 16)
 *	 @pool - the requested memory pool to allocate the memory on.
 *
 * Return value:
 *   a handle to the given context memory
 *
 * Comments:
 *    handle returned only if successful. exits in case of failure to allocate.
 */
gdl_mem_handle_t gdl_mem_alloc_nonull(gdl_context_handle_t ctx_handler, unsigned int size, gdl_mapping_type pool);


/*
 * gdl_mem_free - deallocate the allocated memory handle.
 *
 * Input:
 *   @buffer - handle to a previously allocated context memory handle
 *
 * Comments:
 *   in case a null handler is given the function simply returns
 */
void gdl_mem_free(gdl_mem_handle_t buffer);


/*
 * gdl_mem_handle_to_host_ptr - converts the memory handle to a pointer in the host memory region.
 *
 * Input:
 *   @handle - a memory handle previously allocated
 *
 * Return value:
 * 	 a pointer to the contexts l4 memory that the host can access
 *   in case of failure NULL pointer is returned
 * 
 *
 */
void *gdl_mem_handle_to_host_ptr(gdl_mem_handle_t handle);

/*
 * gdl_task_desc_init - initializes a requested user task
 *
 * Input:
 * 	 @ctx_handler - the id of an hardware context previously allocated
 *   @code_offset - the code offset of the function that the task should execute
 *   @inp - input memory handle
 *   @outp - output memory handle
 *   @comp_flags - completion flags for task
 *    								        (	GSI_TASK_NOTIFY_COMPLETION,
												GSI_TASK_IS_BARRIER,
												GSI_TASK_IS_INITIAL,
												GSI_TASK_NEED_MAPPING,
 *    										)
 *   @apuc_idx - the index of the apuc that the task should run on
 *
 * Output:
 *  @task_desc - a previously allocated user task to be initiated.
 *
 * Return value:
 *   a pointer to an initialized user task
 *   in case of failure NULL pointer is returned
 *
 */
struct gsi_task_desc *gdl_task_desc_init(gdl_context_handle_t ctx_handler, 
										struct gsi_task_desc *task_desc,
										unsigned int code_offset,
										gdl_mem_handle_t inp, 
										gdl_mem_handle_t outp, 
										unsigned char comp_flags, 
										unsigned int apuc_idx);



/*
 * gdl_run_task_timeout - creates, initializes and runs a task synchronously.
 *
 * Input:
 *   @ctx_handler - the id of an hardware context previously allocated
 *   @code_offset - the code offset of the function that the task should execute
 *   @inp - input memory handle
 *   @outp - output memory handle
 * 	 @mem_buf - an array of previously allocated memory handles and their sizes
 * 	 @buf_size - the length of the mem_buf array
 * 	 @apuc_idx - the apuc that the task should be executed on
 *   @ms_timeout - the time in mili seconds a task should wait for completion before aborting ( 0 indicates waiting indefinitely ) 
 *   @map_type - determine the mapping type for the specific task
 *
 * Output:
 * 	 @comp - if task was successfully scheduled, and @comp is provided, the task completion status,
 * 			 or any error is returned in comp.
 * 
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 * 
 * Comments:
 *  The dynamically mapped memory region for the task is determined according to @mem_buf
 *  The comp paramater is optional and mainly for situations when the given timeout of the task has ended and the functions return code is a timeout error,
 *  if comp is given more information can be returned regarding the reason the task has timedout.
 *  In case comp is NULL then the general status will only be returned by the function, with no specific information. 
 * 
 *  Currently comp will only return GDL_TASK_TIMEDOUT.
 *  
 *  GDL_USER_MAPPING - option determines that the task uses the normal user mapping of the AT table.
 * 
 */
int gdl_run_task_timeout(gdl_context_handle_t ctx_handler, 
						unsigned int code_offset, 
						gdl_mem_handle_t inp, 
						gdl_mem_handle_t outp, 
						struct gdl_mem_buffer *mem_buf, 
						unsigned int buf_size, 
						unsigned int apuc_idx,
						gdl_task_comp_status *comp,
						unsigned int ms_timeout,
						gdl_mapping_type map_type);


/* ========= Next we describe functions used to initialize GDL and allocate a card.				 ===========
   ========= These functions are not to for tutorial participants to use, but only for reference ===========  */
/*
 * gdl_init - initializes the GSI device library.
 *
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 *
 * Comments:
 *  gdl_init only needs to be called once, prior to using any GDL functions. However, if you
 *  call gdl_exit, call gdl_init again.
 *
 */
int gdl_init(void);


/*
 * gdl_exit - exits the GSI device library
 *
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 *
 */
int gdl_exit(void);


/*
 * gdl_context_count_get - get the number of hardware contexts installed on the host. 
 *
 * Output:
 *   count - the number of contexts on the host.
 *
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 *
 * 
 */
int gdl_context_count_get(unsigned int *count);


/*
 * gdl_context_desc_get - get information on the requested number of hardware contexts as specified by count.
 *
 * Input:
 *   @count - the number of contexts to receive information of.
 *
 * Output:
 *   ctx_desc - an array of the requested contexts descriptors.
 *
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 */
int gdl_context_desc_get(struct gdl_context_desc *ctx_desc, unsigned int count);


/*
 *  gdl_context_alloc - allocates a specific hardware context to be used by the host.
 *
 * Input:
 *   @ctx_handler - the requested hardware context id as specified in the gdl_context_desc structure.
 *   @const_mapped_size_req - the requested size of the context constantly mapped memory region.
 *
 * Output:
 *   @const_mapped_size_recv - the actual given size of the context constantly mapped memory region
 * 	 @dynamic_mapped_size_recv - the actual given size of the context dynamically mapped memory region 
 * 
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code 
 */

int gdl_context_alloc(gdl_context_handle_t ctx_handler,
						const unsigned long long const_mapped_size_req, 
						unsigned long long *const_mapped_size_recv, 
						unsigned long long *dynamic_mapped_size_recv);

/*
 * gdl_context_free - free an allocated context.
 *
 * Input:
 *   @ctx_handler - the ID of the context previously allocated
 *
 *
 * Return value:
 *   = 0 - OK.
 *   != 0 - error code
 *
 */
int gdl_context_free(gdl_context_handle_t ctx_handler);


#endif /* GSI_LIBGDL_H */
