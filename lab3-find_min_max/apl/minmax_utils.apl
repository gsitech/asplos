/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

APL_C_INCLUDE <gsi/libsys/assert.h>
APL_C_INCLUDE <gsi/libsys/types.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/libgal.h>
APL_C_INCLUDE <gsi/apl_defs.h>
APL_C_INCLUDE <gsi/libgvml.h>
#include "gvml_apl_defs.apl.h"
#include "minmax_utils.apl.h"

/*
 * converts nibble located on nibble_bit of nibble_vr into max nibble mask
 * e.g.  nibble 0x0 -> nibble_mask 0x0001, nibble 0x4 -> nibble_mask 0x001F, nibble 0xF -> nibble_mask 0xFFFF
 * the nibble_mask output is locatet on RL.
 * For entries with mrk16 == 0x0000 the nibble_mask is set to minumum 0x0001.
 */
APL_FRAG from_nibble_to_nibble_mask_max(RN_REG mrk16, RN_REG nibble_vr, SM_REG nibble_bit, RN_REG tmp)
{
	{	// Load nibble (b0..b3) & mrk16 to RL and set GL = b3
		SM_0XFFFF: RL = SB[nibble_vr, mrk16];
		nibble_bit << 3: GL = RL;
	}{	// Set tmp[1..15] = b3 and tmp[0] = 1
		SM_0XFFFF << 1: SB[tmp] = GL;
		SM_0X0001: SB[tmp] = INV_NRL; // tmp[0] = 1
		//set GL = b0
		nibble_bit: GL = RL;
	}{	// Set RL[b3] = b2 (Just to keep it there until we use it)
		nibble_bit << 3: RL = NRL;
		// Set RL[0X5555] = b0
		SM_0X5555: RL = GL;
		//set GL = b1
		nibble_bit << 1: GL = RL;
	}{	// Set RL[SM_0X1111] = b1 | b0
		SM_0X1111: RL |= GL;
		// Set RL[SM_0X2222] = b1
		SM_0X1111 << 1: RL = GL;
		// Set RL[SM_0X4444] = b1 & b0
		SM_0X1111 << 2: RL &= GL;
		//set GL = b2 (which is kept on b3 location)
		nibble_bit << 3: GL = RL;
	}{	// Set RL[SM_0X0101] = b2 | (b1 | b0)
		// Set RL[SM_0X0202] = b2 | b1
		// Set RL[SM_0X0404] = b2 | (b1 & b0)
		SM_0X0707: RL |= GL;
		// Set RL[SM_0X0808] = b2
		SM_0X0101 << 3: RL = GL;
		// Set RL[SM_0X1010] = b2 & (b1 | b0)
		// Set RL[SM_0X2020] = b2 & b1
		// Set RL[SM_0X4040] = b2 & (b1 & b0)
		SM_0X0707 << 4: RL &= GL;
	}{	// Set RL[SM_0X0001] = 1
		// Set RL[SM_0X0002] = b3 | (b2 | (b1 | b0))
		// Set RL[SM_0X0004] = b3 | (b2 | b1)
		// Set RL[SM_0X0008] = b3 | (b2 | (b1 & b0))
		// Set RL[SM_0X0010] = b3 | b2
		// Set RL[SM_0X0020] = b3 | (b2 & (b1 | b0))
		// Set RL[SM_0X0040] = b3 | (b2 & b1)
		// Set RL[SM_0X0080] = b3 | (b2 & (b1 & b0))
		~(SM_0XFFFF << 8): RL = SB[tmp] | NRL;
		// Set RL[SM_0X0100] = b3
		SM_0XFFFF << 9: RL = SB[tmp] & NRL;
		// Set RL[SM_0X0200] = b3 & (b2 | (b1 | b0))
		// Set RL[SM_0X0400] = b3 & (b2 | b1))
		// Set RL[SM_0X0800] = b3 & (b2 | (b1 & b0))
		// Set RL[SM_0X1000] = b3 & b2
		// Set RL[SM_0X2000] = b3 & (b2 & (b1 | b0))
		// Set RL[SM_0X4000] = b3 & (b2 & b1)
		// Set RL[SM_0X8000] = b3 & (b2 & (b1 & b0))
		SM_0X0001 << 8: RL = SB[tmp];
	}
};


/*
 * converts nibble located on nibble_bit of nibble_vr into nibble mask while processing half-bank RSP.
 * Than check the RSP results with the nibble mask and update mrk16.
 * Input for RSP loacted on RL.
 * the nibble_mask output is locatet on RL for next iteration.
 */
APL_FRAG from_nibble_to_nibble_mask_max_with_hb_rsp(RN_REG mrk16, RN_REG nibble_vr, SM_REG nibble_bit, RN_REG tmp0, RN_REG tmp1)
{
	{
		SM_0XFFFF: RL = SB[nibble_vr];
		nibble_bit << 3: GL = RL;
		SM_0XFFFF: SB[tmp0] = RL;

		// Start RSP output - RSP16
		SM_0XFFFF: RSP16 = RL;
	}{
		SM_0XFFFF << 1: SB[tmp1] = GL;
		SM_0X0001: SB[tmp1] = INV_NRL; // SB[tmp1] = 1
		nibble_bit: GL = RL;

		// Continue RSP output - RSP256
		RSP256 = RSP16;
	}{
		nibble_bit << 3: RL = NRL;
		SM_0X5555: RL = GL;
		nibble_bit << 1: GL = RL;

		// Continue RSP output - RSP2K
		RSP2K = RSP256;
	}{
		SM_0X1111: RL |= GL;
		SM_0X1111 << 1: RL = GL;
		SM_0X1111 << 2: RL &= GL;
		nibble_bit << 3: GL = RL;
	}{
		SM_0X0707: RL |= GL;
		SM_0X0101 << 3: RL = GL;
		SM_0X0707 << 4: RL &= GL;

		// Start RSP input
		RSP_START_RET;
	}{
		~(SM_0XFFFF << 8): RL = SB[tmp1] | NRL;
		SM_0XFFFF << 9: RL = SB[tmp1] & NRL;
		SM_0X0001 << 8: RL = SB[tmp1];

		// Continue RSP input - RSP256
		RSP256 = RSP2K;
	}{
		// keep the nibble mask
		SM_0XFFFF   : SB[tmp1] = RL;

		// Continue RSP input - RSP16
		RSP16 = RSP256;
	}{	// get nibble_masks from RSP16
		SM_0XFFFF: RL = SB[tmp0, mrk16] ^ INV_RSP16;
		SM_0XFFFF: GL = RL;
	}{
		RSP_END;
		SM_0XFFFF: SB[mrk16] = GL;
		SM_0X0001: RL = SB[tmp1];
		~SM_0X0001: RL = SB[tmp1] & GL;
	}
};


APL_FRAG hb_rsp_and_update_mrk(RN_REG mrk16, RN_REG mrk_vr, SM_REG mrk)
{
	SM_0XFFFF: RSP16 = RL;
	RSP256 = RSP16;
	RSP2K = RSP256;
	RSP_START_RET;
	RSP256 = RSP2K;
	{
		SM_0XFFFF: RL &= SB[mrk16];
		RSP16 = RSP256;
	}
	{	// get nibble_masks from RSP16
		SM_0XFFFF        : RL ^= INV_RSP16;
		SM_0XFFFF: GL = RL;
	}
	{
		RSP_END;
		mrk: SB[mrk_vr] = GL;
	}
};



void _gvml_mark_minmax_in_hb_u16_t4(enum gvml_mrks_n_flgs mdst, enum gvml_mrks_n_flgs msrc, enum gvml_vr16 data, bool max)
{
	int num_bits = 16;
	int cur_bit = (num_bits - 1) & ~3;
	u16 bits_mask = (u16)((1 << num_bits) - 1);
	u16 min_msk = (u16)(max * 0xFFFF);
	/* Init 16b marker VR and the min_max_val VR */
	apl_set_rn_reg(RN_REG_G0, data);
	apl_set_sm_reg(SM_REG0, min_msk);
	apl_set_sm_reg(SM_REG1, msrc);
	apl_set_sm_reg(SM_REG2, bits_mask);
	RUN_IMM_FRAG_ASYNC(mark_minmax_u16_hb_t4_init(RN_REG mrk16 = RN_REG_T0, RN_REG mrk_vr = RN_REG_FLAGS,
							     SM_REG mrk = SM_REG1, RN_REG tmp_data = RN_REG_T3,
							     RN_REG data = RN_REG_G0, SM_REG min_msk = SM_REG0,
							     SM_REG msb_msk = SM_REG2)
	{
		{
			msb_msk: RL = SB[data];
			~msb_msk: RL = 0;
		}{	// Init tmp_data
			min_msk: SB[tmp_data] = RL;
			~min_msk: SB[tmp_data] = INV_RL;

			// Init 16b marker vr
			mrk: RL = SB[mrk_vr];
			mrk: GL = RL;
		}{
			SM_0XFFFF: SB[mrk16] = GL;
		}
	});

	/* Calc nibble_mask of msb nibble */
	apl_set_sm_reg(SM_REG0, 1 << cur_bit);
	RUN_FRAG_ASYNC(from_nibble_to_nibble_mask_max(mrk16 = RN_REG_T0, nibble_vr = RN_REG_T3, nibble_bit = SM_REG0, tmp = RN_REG_T1));

	for (cur_bit -= 4; cur_bit >= 0; cur_bit -= 4) {
		/* Calc nibble_mask of next nibble while process half-bank RSP for the current nibble mask */
		apl_set_sm_reg(SM_REG0, 1 << cur_bit);
		RUN_FRAG_ASYNC( from_nibble_to_nibble_mask_max_with_hb_rsp(mrk16 = RN_REG_T0, nibble_vr = RN_REG_T3, nibble_bit = SM_REG0, tmp0 = RN_REG_T1, tmp1 = RN_REG_T2));
	}

	/* Process half-bank RSP for the last nibble mask (lsb nibble) and update output marker */
	apl_set_sm_reg(SM_REG1, mdst);
	RUN_FRAG_ASYNC(hb_rsp_and_update_mrk(mrk16 = RN_REG_T0, mrk_vr = RN_REG_FLAGS, mrk = SM_REG1));
}
