/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef MIN_MAX_UTILS_H
#define MIN_MAX_UTILS_H



void _gvml_mark_minmax_in_hb_u16_t4(enum gvml_mrks_n_flgs mdst, enum gvml_mrks_n_flgs msrc, enum gvml_vr16 vsrc, bool max);

#endif /* MIN_MAX_UTILS_H */
