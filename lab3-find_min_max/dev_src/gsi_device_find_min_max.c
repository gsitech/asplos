/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libsys/log.h>
#include <gsi/libgal.h>
#include <gsi/libgvml_element_wise.h>
#include <gsi/libgvml_get_marked_data.h>
#include <gsi/libgvml_min_max.h>
#include <gsi/libgvml_iv.h>
#include <gsi/libgvml_debug.h>
#include <stdlib.h>

#include "../apl/minmax_utils.apl.h"
#include "my_dma.h"
#include "gsi_device_find_min_max.h"

GAL_INCLUDE_INIT_TASK;

/*
 * Welcome to lab 3 of GSI APU coding session.
 * In this task we'll find the input vector limits- min and max values.
 * We will code for unsigned shorts (u16), but this can be easily extended to other types.
 *
 * Recall each core consists 2 APCs, and each APC consists of 8 half-banks.
 * In every half-bank there are 2048 columns, or 16-bit processors. There is no connectivity between half-banks.
 *
 * This task will be done in 3 steps:
 * a. Find the max/min element in every half-bank.
 * b. Retrieve the max/min element from every half-bank for a total of 16 candidate.
 * c. Compare all results, and return the max and min element.
 *
 * Use the GVML functions described in resources/libgvml_ref.h in your solution.
 *
 * Let's go.
 */

GAL_TASK_ENTRY_POINT(find_min_max_task, in, out)
{
	// As in Lab1 - cast the input pointer (in) to common_dev_host* type.
	struct common_dev_host *cmn_handle;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Access the common struct buffer and read the input from host.
	gsi_info("Running %s", cmn_handle->buffer);
	// gvml_init_once() must be invoked once in the beggining of a program, for GVML internal initializations.
	gvml_init_once();

	// Use gal_mem_handle_to_apu_ptr() to assign the address of the input vector to in_A.
	u16 *in_A;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Assign a VR to store the data input.
	enum gvml_vr16 A = GVML_VR16_0;

	// Assign a VMR to be a buffer for the data between L1 and MMB
	enum gvml_vm_reg vm_reg0 = GVML_VM_0;

	// Load the input data pointed to by in_A from the L4 to the MMB.

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * enum gvml_mrks_n_flgs are a set of markers and flags used by the user and GVML library as (1 bit X 32K elements) vectors.
	 * Examples for the use of flags and markers:
	 * - Overflow flag of an addition operation.
	 * - Mark Matched bit-line in a search (as we'll do in lab 4)
	 * - Marker of valid entries (where data is present)
	 * In this task we'll use markers to mark the max/min element in ever half-bank
	 */
	gvml_mrks_t src_mrk = GVML_MRK0;
	gvml_mrks_t dst_mrk = GVML_MRK1;
	/* Since all 32K element of the input vector are possible min/max values set the entire src_mrk vector using gvml_set_m() */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Another programmer (had used an APL fragment to) build a function that finds the min or max value in the half-bank, and marks only that column.
	 * The functionis called _gvml_mark_minmax_in_hb_u16_t4()
	 * Use the function to search for the max value in every half-bank.
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/*
	 * Next, extract the marked data from the MMB and compare the results to find the max.
	 * 1. Use gvml_get_marked_data() to retrieve the maximal element from each half-bank.
	 * 2. Scan all the candidates and save the maximum in max_val.
	 * See the documentation of function gvml_get_marked_data in file resources/libgvml_ref.h
	 */
	u16 candidates[16];
	u16 max_val = 0;
	int num_found;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Store the found max in the common struct in the L4
	cmn_handle->max_found = max_val;

	/*
	 * Now do the same to find the MIN value:
	 * Use _gvml_mark_minmax_in_hb_u16_t4 to mark the Minimal values in each half-bank.
	 * Extract the marked data and find the minimum.
	 */
	u16 min_val = 0xffff;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	cmn_handle->min_found = min_val;

	/*
	 * End of lab 3.
	 * Try compiling your code and running it on the APU server.
	 * For any questions, please contact the guides.
	 */

	return 0; // uccess
}

