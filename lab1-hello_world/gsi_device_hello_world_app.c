/*
 * Copyright (C) 2022, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include "gsi_device_hello_world.h"

GDL_TASK_DECLARE(hello_world_task);

enum { NUM_CTXS = 1 };

/*
 * Welcome to the first lab of applicative programming using real GSI APU hardware.
 * In this lab, We'll learn to allocate memory on the L4, and access it on both host and APU sides.
 * Instructions:
 * During the lab, see the resources/libgal_ref.h and resources/libgdl_ref.h files for reference to the required functions.
 * When you are ready, proceed to the "run hello_world function and follow the instructions".
 */
int run_hello_world(gdl_context_handle_t ctx_id)
{
	gsi_status_t ret = 0;
	/*
	 * First, we will allocate a common struct for sharing data between the host and APU.
	 * The struct common_dev_host is defined in the gsi_device_hello_world.h.
	 */
	size_t buf_size = sizeof(struct common_dev_host); // required size of the struct
	gdl_mem_handle_t cmn_struct_mem_hndl; // A memory handle for the struct on the host side (think of it as a sort of 'pointer')
	/*
	 * Allocate the required memory (buf_size) on the L4,
	 * and assign to cmn_struct_mem_hndl the result memory handle
	 * using gdl_mem_alloc_nonull() (see documentation in resources/libgdl_ref.h)
	 */
	cmn_struct_mem_hndl = gdl_mem_alloc_nonull(ctx_id, buf_size, GDL_CONST_MAPPED_POOL);

	struct common_dev_host *cmn_handle;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Convert the memory handle to struct pointer using gdl_mem_handle_to_host_ptr()


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// For example, let's write a string input and read it later in the APU
	/*
	 * Allocate data in L4 for both input and output, store the memory handle in the struct so both host and APU can access them.
	 * You can choose whatever amount of data you'd like. For example, allocate 100 bytes for a string, or 32768*sizeof(uint16_t) for VR size.
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Allocate data in L4 to all input and outputs
	// cmn_handle->in_mem_hndl = gdl_mem_alloc_nonull(/* fill here and uncomment */);
	// cmn_handle->out_mem_hndl = gdl_mem_alloc_nonull(/* fill here and uncomment */);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/* Next, we'd like to store some data in the input memory we allocated */
	strcpy(cmn_handle->in_buffer, "task of Hello_world"); // For example, direct assignment to the common struct

	/*
	 * In order to access the memory that was assigned to the memory handle "cmn_handle->in_mem_hndl",
	 * we'll need to convert it to a point as we did with the struct above, using gdl_mem_handle_to_host_ptr()
	 */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	uint16_t *l4_ptr;
	// l4_ptr =
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Initiate data in L4 as you wish


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Next, initiate a task to the APU using gdl_run_task_timeout()
	const gdl_task_code_offset_t task = GDL_TASK(hello_world_task);
	ret = gdl_run_task_timeout(ctx_id /* card id */,
							   task /* task to do */,
							   cmn_struct_mem_hndl /* Memory handle to pass to the task */,
							   /* The next parameters are out of the scope of this tutorial, we use the default parameters */
							   GDL_MEM_HANDLE_NULL,
							   GDL_TEMPORARY_DEFAULT_MEM_BUF, GDL_TEMPORARY_DEFAULT_MEM_BUF_SIZE,
							   GDL_TEMPORARY_DEFAULT_CORE_INDEX, NULL,
							   100, GDL_USER_MAPPING
							   );

	/* This kind of task is synchronous. The host will wait until the APU finishes or 100 milliseconds (first to occur)
	 * Go to hello_world-module.c to code the APU task.
	 * When done continue here to read the output.
	 */
	if (0 != ret) { // check errors
		printf("Error during run task: %s (%d)", gsi_status_errorstr(ret), ret);
		if (ret == -ETIMEDOUT) {
			printf(" (time out)");
		}
		putchar('\n');
	}

	/* To finalize this tutorial, read the output buffer and see what outputs we got */
	l4_ptr = gdl_mem_handle_to_host_ptr(cmn_handle->out_mem_hndl);
	printf("\nAPU says: %s\nHere are some values:\n", cmn_handle->out_buffer);

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// print outputs fron l4_ptr


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/* Finally, we'll have to free all allocated data on the L4 using the gdl_mem_free() function */

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Free memory handles


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Example gdl_mem_free() :
	gdl_mem_free(cmn_struct_mem_hndl);

	/* Great, now that you have finished coding this file and hello_world-module.c, go on and try to compile the code and run it on the APU server */

	return ret;
}

static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


/* Main function for the lab exercise. Please do not edit this part. */
int main(int GSI_UNUSED(argc), char *argv[])
{
	gsi_status_t ret = 0;
	unsigned int num_ctxs;
	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	printf("Run Hello World App\n");
	ret = gsi_libsys_init(argv[0], true);
	if (0 != ret) {
		gsi_fatal("gsi_libsys_init failed: %s (%d)", gsi_status_errorstr(ret), ret);
	}

	const gsi_status_t not_a_simulator_status = gsi_status(ENOSYS);
	ret = gsi_sim_create_simulator(NUM_CTXS, g_ctxs);
	if ((not_a_simulator_status != ret) && (0 != ret)) {
		gsi_fatal("gsi_sim_create_simulator failed: %s (%d)", gsi_status_errorstr(ret), ret);
	}

	ret = gdl_init();
	if (0 != ret) {
		gsi_fatal("gdl_init failed: %s (%d)", gsi_status_errorstr(ret), ret);
	}
	ret = gdl_context_count_get(&num_ctxs);
	if (0 != ret) {
		gsi_fatal("gdl_context_count_get failed: %s (%d)", gsi_status_errorstr(ret), ret);
	}
	ret = gdl_context_desc_get(contexts_desc, num_ctxs);
	if (0 != ret) {
		gsi_fatal("gdl_context_desc_get failed: %s (%d)", gsi_status_errorstr(ret), ret);
	}

	gsi_status_t run_status = 0;
	unsigned ctx_id = 0;
	for (ctx_id = 0; ctx_id < num_ctxs; ctx_id++) {
		run_status = gdl_context_alloc(contexts_desc[ctx_id].ctx_id, 0, NULL, NULL);
		if (run_status) {
			printf("Skip card number %u - gdl_context_alloc failed: %s (%d)\n", ctx_id, gsi_status_errorstr(ret), ret);
			continue;
		}
		printf("Running on card number %u\n", ctx_id);
		run_status = run_hello_world(contexts_desc[ctx_id].ctx_id);
		gdl_context_free(contexts_desc[ctx_id].ctx_id);
		break;
	}
	if (ctx_id == num_ctxs) {
		printf("All cards are currently in use. Please try again.\nIf this error persists contact a guide. Sorry for the inconvience.\n");
	}

	ret = gdl_exit();
	if (0 != ret) {
		printf("gdl_exit failed: %s (%d)", gsi_status_errorstr(ret), ret);
		gsi_abort();
	}

	ret = gsi_sim_destroy_simulator();
	if ((not_a_simulator_status != ret) && (0 != ret)) {
		printf("gsi_sim_destroy_simulator failed: %s (%d)", gsi_status_errorstr(ret), ret);
		gsi_abort();
	}

	gsi_libsys_exit();

	if (0 != run_status) {
		printf("\nHello World App Failed\n");
	}
	else {
		printf("\nHello World App Passed\n");
	}

	return run_status;
}
