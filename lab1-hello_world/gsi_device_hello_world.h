/*
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef EXE_COMMON_H
#define EXE_COMMON_H

struct common_dev_host {
	char 			in_buffer[64];
	char 			out_buffer[64];
	uint64_t 		in_mem_hndl;
	uint64_t 		out_mem_hndl;
}__attribute__((packed));

#endif /* EXE_COMMON_H */
