    /*
 * GSI Device Library for Host code running User Tasks remotely on GSI's APU System
 *
 * Copyright (C) 2019, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#ifndef GSI_LIBGAL_H_
#define GSI_LIBGAL_H_

typedef int gsi_prod_status_t;
typedef u64 gal_mem_handle_t;

/*
 * gal_malloc - Allocate memory from L3.
 *
 * Input:
 *         @size - Number of bytes to allocate.
 *
 * Return value:
 *         Pointer to address on L3 - OK.
 *         Pointer with ENOMEM value - allocation failed.
 *
 * Comments:
 *         Validate pointer with GSI_IS_ERR_PTR define.
 *         If size = 0, assert will appear in debug mode.
 */
void *gal_malloc(u32 size);

/*
 * gal_free - Deallocate memory on L3.
 *
 * Input:
 *         @p - pointer to memory.
 */
void gal_free(const void *p);

/*
 * profile functions
 */

/*
 * gal_prof_reset - Reset Performance Monitor count.
 *
 * Comments - use only in simulation, on HW this is an empty function.
 */
//use only in simulator
void gal_prof_reset(void);

/*
 * gal_pm_start - Start Performance Monitor count.
 */
void gal_pm_start(void);

/*
 * gal_pm_stop - Stop Performance Monitor count.
 */
void gal_pm_stop(void);

/*
 * gal_get_pm_cycle_count - Get Performance Monitor cycle count.
 *
 * Input:
 *      @is_live - If false there is a need to call gal_pm_stop before calling this function.
 *                    If true there is no need to call gal_pm_stop before calling this function.
 *
 * Return value:
 *       Number of cycles.
 */
unsigned long long gal_get_pm_cycle_count(bool is_live);

/*
 * gal_get_pm_inst_count - Get Performance Monitor instruction count.
 *
 * Input:
 *      @is_live - If false there is a need to call gal_pm_stop before calling this function.
 *                    If true there is no need to call gal_pm_stop before calling this function.
 *
 * Return value:
 *       Number of instructions.
 */
unsigned long long gal_get_pm_inst_count(bool is_live);


/*
 * function name : gal_mem_handle_to_apu_ptr
 * description: translates a memory handle to an APU address the device can access
 * param: [in]: handle - a memory handle allocated from the L4 APU's memory.
 * return value: void * - a 32-bit address that the APU can access, on failure returns NULL.
 * 
 * comments:
 *  the functions can receive memory handles from both the dynamic memory and the constant memory 
 */

void *gal_mem_handle_to_apu_ptr(gal_mem_handle_t handle);


#endif /* GSI_LIBGAL_H_ */
