/*
 * Copyright (C) 2022, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <gsi/libsys.h>
#include <gsi/libgal.h>
#include <stdlib.h>
#include <string.h>

#include "gsi_device_hello_world.h"

GAL_INCLUDE_INIT_TASK;

/*
 * Welcome to the first lab of applicative programming using real GSI APU hardware.
 * In this lab, we'll read the input data passed to us from the host. Process it and write back the output data.
 * Let's start!
 */

/* This is the way we define an APU task, notice the name of the task is "hello_world", the input parameters are void* to the memory handles passed by the host
 * Notice that on the host side we set:
 *     const gdl_task_code_offset_t task = GDL_TASK(hello_world_task);
 * for the task name.
 * The memory handle "cmn_struct_mem_hndl" which we passed as parameter to gdl_run_task_timeout() was
 * converted to a void* pointer, which we will cast to our struct type.
 */
GAL_TASK_ENTRY_POINT(hello_world_task, in, out)
{
	// First, we cast "in" to a (struct common_dev_host *) type so we can access the data in it
	struct common_dev_host *cmn_handle = (struct common_dev_host *)in;
	// Printing to the APU log is done by the gsi_info() function, which behaves similarly to printf()
	gsi_info("I'm in APU! Host says: %s", cmn_handle->in_buffer);

	// In order to access pointers we use gal_mem_handle_to_apu_ptr()
	u16 *in_data, *out_data; // Change type if necessary

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// in_data = gal_mem_handle_to_apu_ptr(/* Insert here the input memory handle */);
	// out_data = gal_mem_handle_to_apu_ptr(/* Insert here the output memory handle */);
	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/* The L3 memory is the working memory (heap) for the APU. It is much faster to access than the L4.
	 * Sometimes we'll want to allocate some data on the L3 during the task execution.
	 * That's done using the gal_malloc() function.
	 * Allocate the same amount of memory as your input on the L3
	 */
	u16 *L3_ptr;

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// allocate memory in L3


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Next, read the input data and write it to the L3/
	gsi_info("I got the following data from the Host: ");

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Print your data using gsi_info
	// Copy the data to L3 for faster access during processing


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	/* Finaly to some processing on the input data, and write it output to out_data */
	gsi_info("I'm writing the following data back to the host");

	// ******** WRITE YOUR CODE UNDER THIS LINE ************************
	// Process input data, now on L3
	// store output data


	// ******** WRITE YOUR CODE ABOVE THIS LINE ************************

	// Write an output string for the host
	strcpy(cmn_handle->out_buffer, "Hello from APU hello world, computed max function for you!");

	// Free allocated memory in L3
	gal_free(L3_ptr);

	return 0; // success
	/*
	 * That's it for now, we have accessed the input data, and written to the output.
	 * In the next labs, we'll see how to read and write data from/to the associative memory, and how to process that data.
	 * Now go back to hello_world.c and finish the function
	 */
}
